from django.conf.urls import url, include
from django.contrib.auth.views import LogoutView
from .views import login_page, register_page, guest_login_view

urlpatterns = [
    url(r'^login/$', login_page, name="login_page"),
    url(r'^logout/$', LogoutView.as_view(), name="logout_page"),
    url(r'^register/$', register_page, name="register_page"),
    url(r'^guest-login/$', guest_login_view, name="guest_login"),
]
