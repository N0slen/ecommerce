from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, get_user_model
from .forms import ContactForm


def home_page(request):
    template = 'homepage/homepage.html'
    ctx = {}
    return render(request, template, ctx)


def about_page(request):
    template = 'about/about.html'
    ctx = {}
    return render(request, template, ctx)


def contact_page(request):
    template = 'contact/contact.html'
    form = ContactForm(request.POST or None)
    if form.is_valid():
        print(form.cleaned_data)
    ctx = {
        'contact_form': form
    }
    return render(request, template, ctx)

    """     if request.method == 'POST':
        print(request.POST.get('fullname'))
        print(request.POST.get('email')) """
