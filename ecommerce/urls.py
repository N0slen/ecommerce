from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from .views import home_page, about_page, contact_page

urlpatterns = [
    url(r'^contact/$', contact_page, name="contact_page"),
    url(r'^about/$', about_page, name="about_page"),
    url(r'^$', home_page, name="home_page"),
    url(r'^accounts/', include('accounts.urls', namespace='accounts_page')),
    url(r'^products/', include('products.urls', namespace='products')),
    url(r'^cart/', include('carts.urls', namespace='cart_page')),
    url(r'^search/', include('search.urls', namespace='search_page')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + \
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + \
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
