
from django.shortcuts import render
from products.models import Product
from django.views.generic import ListView


class SearchProductView(ListView):
    template_name = "search/search_view.html"

    def get_context_data(self, *args, **kwargs):
        context = super(SearchProductView, self).get_context_data(
            *args, **kwargs)
        query = self.request.GET.get("q")
        # on template we can just use 'query' instead of 'request.GET.q'
        context['query'] = query
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        query = request.GET.get('q')
        if query is not None:
            return Product.objects.search(query)
        return Product.objects.none()
