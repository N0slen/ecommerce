from django.conf.urls import url
from .views import (
    ProductListView,
    ProductDetailSlugView,
)


urlpatterns = [
    url(r'^$', ProductListView.as_view(), name='products_list'),
    url(r'^product/(?P<slug>[\w-]+)/$',
        ProductDetailSlugView.as_view(), name="detailproduct"),
]
