from django.shortcuts import render, get_object_or_404
from django.http import Http404
from django.views.generic import ListView, DetailView
from .models import Product
from carts.models import Cart


class ProductFeaturedListView(ListView):
    template_name = "products/product_list_view.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.get_featured()


class ProductFeaturedDetailView(DetailView):
    template_name = "products/featured_detail.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.get_featured()


class ProductListView(ListView):
    # model = MODEL_NAME
    template_name = "products/product_list_view.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.all()


"""     def get_context_data(self, **kwargs):
        context = super(Product, self).get_context_data(**kwargs)
        return context """


def product_list_view(request):
    queryset = Product.objects.all()
    template_name = "products/product_list_view.html"
    ctx = {
        'object_list': queryset
    }
    return render(request, template_name, ctx)


class ProductDetailSlugView(DetailView):
    queryset = Product.objects.all()
    template_name = "products/product_detail_view.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailSlugView,
                        self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_object(self, *arg, **kwars):
        request = self.request
        slug = self.kwargs.get('slug')
        try:
            instance = get_object_or_404(Product, slug=slug, active=True)
        except Product.DoesNotExist:
            raise Http404("Not Found...")
        except Product.MultipleObjectsReturned:
            qs = Product.objects.filter(slug=slug, active=True)
            instance = qs.first()
        except:
            raise Http404("uHHh?!")
        return instance


class ProductDetailView(DetailView):
    template_name = "products/product_detail_view.html"

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(
            *args, **kwargs
        )
        # context['asd] = 123 # we can add to context here
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        return Product.objects.filter(pk=pk)


"""     def get_object(self, *arg, **kwars):
        request = self.request
        pk = self.kwargs.get('pk')
        instance = Product.objects.get_by_pk(pk)
        if instance is None:
            raise Http404("Product doesnt exist")
        return instance """


def product_detail_view(request, pk):
    # obj = get_object_or_404(Product, pk=pk)
    template_name = "products/product_detail_view.html"
    instance = Product.objects.get_by_pk(pk)
    if instance is None:
        raise Http404("Product doesnt exist")

    ctx = {
        'object': instance
    }
    return render(request, template_name, ctx)
